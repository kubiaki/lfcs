If you are a sysadmin working with Unix based systems is very important to you to know how to search for files, and two important commands that will help you with that are:

➜  ~ find
usage: find [-H | -L | -P] [-EXdsx] [-f path] path ... [expression]
       find [-H | -L | -P] [-EXdsx] -f path [path ...] [expression]

and:

➜  ~ locate
usage: locate [-0Scims] [-l limit] [-d database] pattern ...

default database: `/var/db/locate.database' or $LOCATE_PATH

The options listed below are the options that I use most in my daily activities, but you can find more options typing the following in your terminal:

➜  ~ man find

You can also combine commands.
find command usage

NAME
     find -- walk a file hierarchy

SYNOPSIS
     find [-H | -L | -P] [-EXdsx] [-f path] path ... [expression]
     find [-H | -L | -P] [-EXdsx] -f path [path ...] [expression]

DESCRIPTION
     The find utility recursively descends the directory tree for each path listed, evaluating an expression (composed of the ``primaries'' and ``operands''
     listed below) in terms of each file in the tree.

Looking for file name

if you are looking for a file using the find command, the basic syntax would be:

➜  ~    find $PATH -name "$FILENAME"

However, you can improve the search just changing -name for -iname because Linux, different of Windows (for instance) is case sensitive and -iname will perform the same search as -name, however ignoring case sensitive, like this:

➜  ~    find $PATH -iname "$FILENAME"

You can also find everything in a directory except the file you informed using -not:

➜  ~    find $PATH -not -name $FILENAME

Or using -iname:

➜  ~    find $PATH -not -iname $FILENAME

In this way, everything in that folder, except the file you declared.
Looking for file extension

With find command you can also look for an particular file extension and this can be done as follow:

➜  ~ find $PATH -type f -name "*.txt"

Looking for directories

Another option we have with find command is find for directories instead of files or file extensions:

➜  ~ find $PATH -type d -name "log"

and here you can use -iname again:

➜  ~ find $PATH -type d -iname "log"

Looking for file based on the file size

Using -size we can look for files gatherer than or smaller than the size you specify.

Let say you want to find a file that gather than 2 bytes:

➜  ~ find $PATH -size +2c

Let say you want to find a file smaller than 3 megabyte:

➜  ~ find $PATH -size -3M

Or you can us k for kilobyte.
Looking for files based on timestamps

You can use -mtime to find files based on timestamps.

If you want to find a file that was created greater than a day:

➜  ~ find $PATH -type f -mtime 1

or less than a day:

➜  ~ find $PATH -type f -mtime -1

Looking for files based on user

If you want to find all the files are owned by a particular user:

➜  ~ find $PATH -user $USER

As I mentioned before, those are a few options of the find command that I think is handy to know.

Thank you.